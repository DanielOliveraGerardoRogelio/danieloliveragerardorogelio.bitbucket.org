var Calendario = (function(){
  

    var getEventos = function(){
      var eventos = [];
      gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 10,
        'orderBy': 'startTime'
      }).then(function(response) {
        var events = response.result.items;
        //appendPre('Upcoming events:');

        if (events.length > 0) {
          for (i = 0; i < events.length; i++) {
            var event = events[i];
            eventos.push(event);
            var when = event.start.dateTime;
            if (!when) {
              when = event.start.date;
            }
            //appendPre(event.summary + ' (' + when + ')')
          }
        } else {
          appendPre('No upcoming events found.');
        }
      });

      return eventos;
    };

    var mostrarEventos = function(arregloEventos){
      //var arregloEventos = Calendario.getEventos();

      var contenedorEventos = document.getElementById('contenedorEventos');
      contenedorEventos.setAttribute('class','collection');
      var evento;

      var query = document.getElementById('query');

      for(var i = 0; i<arregloEventos.length; i++){
        evento = arregloEventos[i];
        var summaryEvent = document.createElement('div');
        summaryEvent.setAttribute('class','collection-item');

        var texto = document.createTextNode(arregloEventos[i].summary);

        summaryEvent.appendChild(texto);

        
        
        /*summaryEvent.onclick  = function(event){
          var cont = document.getElementById('contenedorDetalles');
          cont.innerHTML = '';
          mostrarDetalleEvento(evento);
        }*/
        
        contenedorEventos.appendChild(summaryEvent);
      }

      //return arregloEventos;

    }

    var asignarEventos = function(eventos){
      //var eventos = getEventos();
      
      var elementos = document.getElementsByClassName('collection-item');
      var query = document.getElementById('query');

      /*for(var i = 0; i< elementos.length; i++){
        var ev;
        elementos[i].onclick = function(event){
          ev = eventos[i];
          var cont = document.getElementById('contenedorDetalles');
          cont.innerHTML = '';
          //Calendario.mostrarDetalleEvento(eventos[i]);
          query.value = ev.summary;
        }
      }*/

      elementos[0].onclick = function(event){
        var cont = document.getElementById('contenedorDetalles');
        cont.innerHTML = '';
        //Calendario.mostrarDetalleEvento(eventos[0]);
        query.value = eventos[0].summary;
      }

      elementos[1].onclick = function(event){
        var cont = document.getElementById('contenedorDetalles');
        cont.innerHTML = '';
        //Calendario.mostrarDetalleEvento(eventos[1]);
        query.value = eventos[1].summary;
      }

      elementos[2].onclick = function(event){
        var cont = document.getElementById('contenedorDetalles');
        cont.innerHTML = '';
        //Calendario.mostrarDetalleEvento(eventos[2]);
        query.value = eventos[2].summary;
      }

      elementos[3].onclick = function(event){
        var cont = document.getElementById('contenedorDetalles');
        cont.innerHTML = '';
        //Calendario.mostrarDetalleEvento(eventos[3]);
        query.value = eventos[3].summary;
      }
      
      elementos[4].onclick = function(event){
        var cont = document.getElementById('contenedorDetalles');
        cont.innerHTML = '';
        //Calendario.mostrarDetalleEvento(eventos[3]);
        query.value = eventos[4].summary;
      }
    }

    var mostrarDetalleEvento = function(evento){
      var contendorDetalle = document.getElementById('contenedorDetalles');

      var creador = "Creado por: " + evento.creator.displayName;
      var summary = "Asunto: " + evento.summary;
      var id = document.createTextNode("ID: " + evento.id);
      var inicio = "Inicio: " + evento.start.date;
      var fin = "Termino: " + evento.end.date;


      var div0 = document.createElement('div');
      div0.setAttribute('class', 'row');

      var div1 = document.createElement('div');
      div1.setAttribute('class', 'col s12 m6');

      var div2 = document.createElement('div');
      div2.setAttribute('class', 'card blue-grey darken-1');

      var div3 = document.createElement('div');
      div3.setAttribute('class', 'card-content white-text');

      var span = document.createElement('span');
      span.setAttribute('class', 'card-title');
      span.textContent = summary;

      var pcreador = document.createElement('p');
      pcreador.textContent = creador;

      var pinicio = document.createElement('p');
      pinicio.textContent = inicio;

      var pfin = document.createElement('p');
      pfin.textContent = fin;

      var cardaction = document.createElement('div');
      cardaction.setAttribute('class', 'card-action');

      var alink = document.createElement('a');
      alink.setAttribute('href', evento.htmlLink);
      alink.textContent = 'Ir a Evento'

      var ayoutube = document.createElement('a');
      ayoutube.textContent = "Youtube";
            
      span.appendChild(pcreador);
      span.appendChild(pinicio);
      span.appendChild(pfin);

      div3.appendChild(span);

      cardaction.appendChild(alink);
      cardaction.appendChild(ayoutube);

      div2.appendChild(div3);
      div2.appendChild(cardaction);

      div1.appendChild(div2);
      div0.appendChild(div1);

      contendorDetalle.appendChild(div0);

    }
    
    var youtubeSearch = function(){
      var inputQuery = document.getElementById('query');
      var query = inputQuery.value;
      var results=[];
      //var q = $('#query').val();
      var q = query;
      var request = gapi.client.youtube.search.list({
        q: q,
        part: "snippet",
        type: "video",
        maxResults: 10,
        order: "viewCount"
      });
    
      request.execute(function(response) {
        results.push(response.result);
        var str = JSON.stringify(response.result);
        //$('#search-container').html('<pre>' + str + '</pre>');
        //console.log(response.result);
        //return response.result;
      });

      return results;

    }

    

    var mostrarVideos = function(arregloVideos){

      var contenedorYoutube = document.getElementById('contenedor-youtube');
      contenedorYoutube.innerHTML="";
      

      for(var i = 0; i<arregloVideos[0].items.length; i++){
        var videoid = arregloVideos[0].items[i].id.videoId;
        
        var titlevideo = arregloVideos[0].items[i].snippet.title;

        var divItem = document.createElement('div');
        divItem.setAttribute('class', 'item');
        var h1 = document.createElement('h3');
        h1.textContent = titlevideo;

        divItem.appendChild(h1);
        

        var iframe = document.createElement('iframe');
        iframe.setAttribute('class', 'video w100');
        //iframe.setAttribute('width', '360');
        //iframe.setAttribute('height', '240');
        iframe.setAttribute('src', '//www.youtube.com/embed/'+videoid);
        iframe.setAttribute('frameborder', '2');
        iframe.setAttribute('allowFullscreen', true);

        divItem.appendChild(iframe);
        contenedorYoutube.appendChild(divItem);
      } 
    }

    var youtubeSearchVideo = function(id){
      var results =[];
      var request = gapi.client.youtube.videos.list({
        part: "recordingDetails",
        id: id
      });
    
      request.execute(function(response) {
        results.push(response.result);
        var str = JSON.stringify(response.result);
        //$('#search-container').html('<pre>' + str + '</pre>');
        //console.log(response.result);
        return response.result;
      });

      return results;

    }

    var crearTabla = function(elementos){

      var body = document.getElementById('div-tabla');
 
      var selector = document.getElementById('selector')

      var cantidad = parseInt(selector.value);

      var filas = cantidad /5;
      
      var tabla   = document.createElement("table");
      var tblBody = document.createElement("tbody");
    
      
      for (var i = 0; i < filas; i++) {
        var hilera = document.createElement("tr");
    
        for (var j = 0; j < 5; j++) {
          var celda = document.createElement("td");
          celda.setAttribute('class', 'celda');
          
          celda.appendChild(elementos[j]);
          hilera.appendChild(celda);
        }
    
        tblBody.appendChild(hilera);
      }
    
      tabla.appendChild(tblBody);
      body.appendChild(tabla);
      tabla.setAttribute("border", "2");
    }

    var agregarVideosTabla = function(arregloVideos){
      var celdas = document.getElementsByTagName('td');

      for(var i = 0; i<celdas.length; i++){
        var videoid = arregloVideos[0].items[i].id.videoId;
        
        var titlevideo = arregloVideos[0].items[i].snippet.title;

        var h4 = document.createElement('h4');
        h4.textContent = titlevideo;

        var iframe = document.createElement('iframe');
        iframe.setAttribute('class', 'video w100');
        //iframe.setAttribute('width', '360');
        //iframe.setAttribute('height', '240');
        iframe.setAttribute('src', '//www.youtube.com/embed/'+videoid);
        iframe.setAttribute('frameborder', '2');
        iframe.setAttribute('allowFullscreen', true);

        celdas[i].appendChild(h4);
        celdas[i].appendChild(iframe);
      }

    }
  

    return{
        "getEventos": getEventos,
        "mostrarEventos": mostrarEventos,
        "mostrarDetalleEvento": mostrarDetalleEvento,
        "asignarEventos": asignarEventos,
        "youtubeSearch": youtubeSearch,
        "mostrarVideos": mostrarVideos,
        "crearTabla": crearTabla,
        "agregarVideosTabla": agregarVideosTabla 
    }

})();